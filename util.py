import cv2
import mediapipe as mp
import math


class Util:
    def vector_2d_angle(v1, v2):
        '''
            求解二维向量的角度
        '''
        v1_x = v1[0]
        v1_y = v1[1]
        v2_x = v2[0]
        v2_y = v2[1]
        try:
            angle_ = math.degrees(math.acos(
                (v1_x * v2_x + v1_y * v2_y) / (((v1_x ** 2 + v1_y ** 2) ** 0.5) * ((v2_x ** 2 + v2_y ** 2) ** 0.5))))
        except:
            angle_ = 65535.
        if angle_ > 180.:
            angle_ = 65535.
        return angle_

    def is_straight(angle):
        return angle < 40

    def get_distance(point1, point2):
        return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

    def \
            is_in_rectangle(target_point, rectangle):
        if target_point[0] > rectangle[0] and target_point[0] < rectangle[2] and target_point[1] > rectangle[1] and \
                target_point[1] < rectangle[3]:
            return True
        return False
