
import multiprocessing
import sys
import tkinter
from identify import *
from tkinter.filedialog import *
from tkinter import messagebox
from win32com.client import Dispatch
import win32con
import win32api
import time
import os


def control_page_up():
    win32api.keybd_event(38, 0, 0, 0)  # 代表按下f键
    time.sleep(0.02)
    win32api.keybd_event(38, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键


def control_page_down():
    win32api.keybd_event(40, 0, 0, 0)  # 代表按下f键
    time.sleep(0.02)
    win32api.keybd_event(40, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键


def control_ppt_begin():
    win32api.keybd_event(16, 0, 0, 0)  # 代表按下f键
    win32api.keybd_event(116, 0, 0, 0)  # 代表按下f键
    time.sleep(0.02)
    win32api.keybd_event(116, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键
    win32api.keybd_event(16, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键


def control_ppt_end():
    win32api.keybd_event(27, 0, 0, 0)  # 代表按下f键
    time.sleep(0.02)
    win32api.keybd_event(27, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键


def control_open_pencil():
    win32api.keybd_event(17, 0, 0, 0)  # 代表按下f键
    win32api.keybd_event(82, 0, 0, 0)  # 代表按下f键
    time.sleep(0.02)
    win32api.keybd_event(17, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键
    win32api.keybd_event(82, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放f键


def control_writing(m, position_x, position_y):
    m.move(position_x, position_y)


def control_thread(control_number, array, over_flag):
    last_time = 0.0
    step = 0
    mouse = PyMouse()
    test_time1 = time.time()
    count = 0
    while 1:
        count += 1
        # print("step = " + str(step))
        # print("control_thread")
        now_time = time.time()
        control_flag = control_number.value
        # os._exit(0)
        if control_flag == 7:
            # test_time2 = time.time()
            # fps = count / (test_time2 - test_time1)
            # print("count2 = " + str(count))
            # print("time = " + str(test_time2 - test_time1))
            # print("fps_control = " + str(fps))
            over_flag.value = 1
            sys.exit(0)
        if step == 2:
            if control_flag == 4:
                position_x = int(mouse.screen_size()[0] / array[2] * array[0])
                position_y = int(mouse.screen_size()[1] / array[3] * array[1])
                control_writing(mouse, position_x, position_y)
            elif control_flag == 5:
                step = 1
                control_ppt_end()
                last_time = now_time
                continue
        if now_time - last_time < 1:
            continue
        last_time = now_time
        # print("control_flag2 = " + str(control_flag))
        # # print("main.step = " + str(step))
        if step == 0:
            if control_flag == 1:
                control_ppt_begin()
                step = 1
        elif step == 1:
            if control_flag == 2:
                control_page_up()
                # print("control_flag == 2")
            elif control_flag == 3:
                control_page_down()
                # print("control_flag == 3")
            elif control_flag == 4:
                control_open_pencil()
                step = 2
            elif control_flag == 6:
                control_ppt_end()
                step = 0


def identify_thread(control_number, array, over_flag):
    identify = Identify(control_number, array)
    identify.begin(over_flag)


def open_file():
    file_path = askopenfilename(title=u'选择文件')
    if not open_ppt(file_path):
        return False
    control_number = multiprocessing.Value('i', 0)
    array = multiprocessing.Array('i', 4)
    over_flag = multiprocessing.Value('i', 0)
    p1 = multiprocessing.Process(target=identify_thread, args=(control_number, array, over_flag))
    p2 = multiprocessing.Process(target=control_thread, args=(control_number, array, over_flag))
    p1.start()
    p2.start()
    return True


def judge_ppt(file_path):
    string = file_path
    string = string.split('.')
    if len(string) <= 0:
        return False
    string = string[len(string) - 1]
    if string.find('ppt') != -1:
        return True
    return False


def open_ppt(file_path):
    if not len(file_path):
        messagebox.showinfo("选择失败", "您尚未选择ppt文件，请重新选择")
        return False
    if not judge_ppt(file_path):
        messagebox.showinfo("选择失败", "该文件非ppt文件，请重新选择")
        return False
    ppt = Dispatch('PowerPoint.Application')
    ppt.Visible = 1  # 后台运行
    ppt.DisplayAlerts = 0  # 不显示，不警告
    ppt.Presentations.Open(file_path)
    return True


if __name__ == '__main__':
    window = tkinter.Tk()
    window.title("会议PPT选择")
    window.geometry("200x100")
    bt1 = tkinter.Button(window, text='打开文件', width=15, height=15, command=open_file)
    bt1.pack()
    window.mainloop()
